#ifndef IMAGE
#define IMAGE
#include "pixel.h"
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct image* create_image(uint64_t width, uint64_t height);
void destroy_image(struct image* img);
void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel color);
struct pixel get_pixel(const struct image* img, uint64_t x, uint64_t y);
#endif
