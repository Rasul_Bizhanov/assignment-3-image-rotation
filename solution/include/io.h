#ifndef IO
#define IO


#include "image.h"
#include <stdio.h>
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR_OPEN,
    READ_ERROR_MEMORY,
};


enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_ERROR_OPEN,
};

enum read_status read(const char* filename, struct image** img);
enum write_status write(const char* filename, const struct image* img);
#endif
