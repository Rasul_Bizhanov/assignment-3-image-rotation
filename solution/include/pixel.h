#ifndef PIXEL
#define PIXEL
#include <stdint.h>
#pragma pack(push, 1)
struct __attribute__((packed)) pixel { uint8_t b, g, r; };
#pragma pack(pop)
#endif
