#ifndef ROTATE
#define ROTATE
#include "image.h" 
struct image* rotate( struct image *source);
struct image* rotate_angle(struct image *source, int angle);
#endif
