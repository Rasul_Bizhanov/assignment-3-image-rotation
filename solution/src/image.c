#include "image.h"

struct image* create_image(uint64_t width, uint64_t height) {
    struct image* image = malloc(sizeof(struct image));

    if (image == NULL) {
        return NULL;
    }
    image->width = width;
    image->height = height;
    image->data = (struct pixel*)malloc(width * height * sizeof(struct pixel));

    if (image->data == NULL) {
        free(image);
        return NULL;
    }
    for (uint64_t i = 0; i < width * height; ++i) {
        image->data[i].r = 0;
        image->data[i].g = 0;
        image->data[i].b = 0;
    }

    return image;
}


void destroy_image(struct image* img) {
    free(img->data);
    free(img);
}

void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel color) {
    if (x < img->width && y < img->height) {
        img->data[y * img->width + x] = color;
    }
}
struct pixel get_pixel(const struct image* img, uint64_t x, uint64_t y) {
    if (x < img->width && y < img->height) {
        return img->data[y * img->width + x];
    }
    struct pixel black = { 0, 0, 0 };
    return black;
}

