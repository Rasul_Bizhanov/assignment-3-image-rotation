#include "io.h"
#include "bmp_header.h"

uint64_t calculate_padding(uint64_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum write_status to_bmp(FILE* out, struct image const* img) {

    struct bmp_header header;

    header.bfType = 0x4D42;
    header.bfileSize = (int32_t)(img->width * img->height * sizeof(struct pixel) + sizeof(header) + img->height * calculate_padding(img->width));
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = (uint32_t)img->width;
    header.biHeight = (uint32_t)img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (uint32_t)(img->width * img->height * sizeof(struct pixel));
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    uint64_t padding = calculate_padding(img->width);
    for (uint64_t i = 0; i < img->height; ++i) {

        if (fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (padding > 0) {  
            if (fseek(out, (long)padding, SEEK_CUR) != 0) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}



enum read_status from_bmp(FILE* in, struct image** img) {
    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    *img = create_image(header.biWidth, header.biHeight);
    if (img == NULL) {
        return READ_ERROR_MEMORY;
    }
    if (!(*img)->data) {
        return READ_ERROR_MEMORY;
    }

    uint64_t padding = calculate_padding((*img)->width);

    for (uint64_t i = 0; i < (*img)->height; ++i) {
        for (uint64_t j = 0; j < (*img)->width; ++j) {
            struct pixel color;
            if (fread(&color, sizeof(struct pixel), 1, in) != 1) {
                destroy_image(*img);
                return READ_INVALID_BITS;
            }
            set_pixel((*img), j, i, color);
        }
        fseek(in, (long)padding, SEEK_CUR);
    }
    return READ_OK;
}


enum read_status read(const char* filename, struct image** img) {
	FILE* file = fopen(filename, "rb");
	if (!file) {
		return READ_ERROR_OPEN;
	}
	enum read_status status = from_bmp(file, img);
	fclose(file);
	return status;
}
enum write_status write(const char* filename, const struct image* img) {
	FILE* file = fopen(filename, "wb");
	if (!file) {
		return WRITE_ERROR_OPEN;
	}
	enum write_status status = to_bmp(file, img);

	fclose(file);
	return status;
}
