#include "io.h"
#include "rotate.h"

int main( int argc, char* argv[]) {
    printf("something");
    if (argc != 4) {
        printf("Usage: %s <source - image> <transformed - image> <angle>\n", argv[0]);
        return 1;
    }

    char* input_filename = argv[1];
    char* output_filename = argv[2];
    
    struct image* original_image;
    enum read_status read_result = read(input_filename, &original_image);

    if (read_result != READ_OK) {
        printf("Error reading the input file. Status: %d\n", read_result);
        return 1;
    }
    struct image* rotate_image = rotate_angle(original_image, atoi(argv[3]));

    enum write_status write_result = write(output_filename, rotate_image);
    destroy_image(rotate_image);
    if (write_result != WRITE_OK) {
        printf("Error writing to the output file. Status: %d\n", write_result);
        return 1;
    }

    //free(&rotate_image);

    return 0;
}
