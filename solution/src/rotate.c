#include "rotate.h"
struct image* rotate(struct image* source) {
    struct image* img = create_image(source->height, source->width);

    for (uint64_t i = 0; i < source->height; ++i) {
        for (uint64_t j = 0; j < source->width; ++j) {
            set_pixel(img, i, source->width - j - 1, get_pixel(source, j, i));
        }
    }
    destroy_image(source);
    return img;
}

struct image* rotate_angle(struct image* source, int angle) {
    for (int i = 0; i < ((360 + angle) / 90); i++) {
        source = rotate(source);
    }
    return source;
}
